#
# ~/.bashrc
#
#/etc/bash.bashrc is read fist to get cutom color prompt
#
# DO NOT sent env's here. use ~/.bash_profile
#

#shopt -s checkwinsize

#alias ls='ls --color=auto'
alias lla='ls -lAhF'
alias ll='ls -lAhF'
alias llt='ls -lhAt'
alias suspend='systemctl suspend'
alias hist=history

alias py="python -q"
alias find_pacnew="sudo find /etc -name '*.pacnew'"

# systemd: list process by service ownership
alias psc='ps xawf -eo pid,user,cgroup,args'

# emacs as root
alias E="SUDO_EDITOR=\"emacs\" sudoedit"

# tab completion for sudo
complete -cf sudo

#set -o ignoreeof

set noclobber

# # RVM bash completion
# [[ -r /usr/local/rvm/scripts/completion ]] && . /usr/local/rvm/scripts/completion

# Git tab completion
source /usr/share/git/completion/git-completion.bash

# git prompt
source /usr/share/git/completion/git-prompt.sh
PS1='\[\e[0;32m\]\h \w\n$(__git_ps1 "(%s)")\$ \[\e[0m\]'

#PS1="$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]\h'; else echo '\[\033[01;32m\]\h'; fi)\[\033[01;36m\] \w\n\$([[ \$? != 0 ]] && echo \"\[\033[01;31m\]:(\[\033[01;36m\] \")\\$\[\033[00m\] "

# $ cdp2 <python module name>
# takes you to the directory where the module is defined
cdp2 () {
    cd "$(python2 -c "import os.path as _, ${1}; \
            print(_.dirname(_.realpath(${1}.__file__[:-1])))"
        )"
}

# $ cdp <python module name>
# takes you to the directory where the module is defined
cdp () {
    cd "$(python -c "import os.path as _, ${1}; \
            print(_.dirname(_.realpath(${1}.__file__[:-1])))"
        )"
}

#export AWS_CONFIG_FILE=/home/scott/aws/aws_creds.txt

# ansible
export ANSIBLE_HOSTS='/etc/ansible/ec2.py'
export EC2_INI_PATH='/etc/ansible/ec2.ini'

export HISTTIMEFORMAT="%d/%m/%y %T "

# Python virtualenvwrapper
export WORKON_HOME=~/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python2
source /usr/bin/virtualenvwrapper.sh

export PATH="$PATH:$HOME/bin"
export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"
