#
# ~/.bash_profile
#

PATH=~/bin:$PATH

[[ -f ~/.bashrc ]] && . ~/.bashrc

# [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

#source ~/zenti/ops/scripts/ssh_config_generator/bash-complete.sh

# # Hashicorp tools
# PATH=$PATH:/usr/local/bin/packer:/usr/local/bin/terraform:/usr/local/bin/otto

export PYTHONPATH="./.pip:$PYTHONPATH"

export PATH="$HOME/.local/bin:$PATH" # for pipenv
